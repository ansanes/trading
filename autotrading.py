import os
import datetime
from ib.ext.Contract import Contract
from ib.ext.Order import Order
from ib.opt import Connection, message
from random import randint
import schedule
import time
import glob,os

ordersPath = "c:/lightspeeddata/"
orders = []
order_id = 4
tws_conn = None
currentPosition = []
symbolsToCancel = []
cancelAllOrdersSet = False
cancelOrdersReached = False
posted_orders = []

class Position:
    def __init__(self, symbol, type, amount, price,lastPrice):
        self.symbol = symbol
        self.averagePrice = price
        self.type = type
        self.lastPrice = lastPrice
        self.amount = amount

class MyOrder:
    def __init__(self, symbol, type, price, amount):
        self.symbol = symbol
        self.price = price
        self.type = type
        self.amount = amount


def error_handler(msg):
    """Handles the capturing of error messages"""
    print "Server Error: %s" % msg


def reply_handler(msg):
    """Handles of server replies"""
    print "Server Response: %s, %s" % (msg.typeName, msg)


def create_contract(symbol, sec_type, exch, prim_exch, curr):
    """Create a Contract object defining what will
    be purchased, at which exchange and in which currency.

    symbol - The ticker symbol for the contract
    sec_type - The security type for the contract ('STK' is 'stock')
    exch - The exchange to carry out the contract on
    prim_exch - The primary exchange to carry out the contract on
    curr - The currency in which to purchase the contract"""
    contract = Contract()
    contract.m_symbol = symbol
    contract.m_secType = sec_type
    contract.m_exchange = exch
    contract.m_primaryExch = prim_exch
    contract.m_currency = curr
    return contract


def create_order(order_type, action,quantity, limit_price):
    """Create an Order object (Market/Limit) to go long/short.

    order_type - 'MKT', 'LMT' for Market or Limit orders
    quantity - Integral number of assets to order
    action - 'BUY' or 'SELL'"""
    order = Order()
    order.m_orderType = order_type
    order.m_totalQuantity = quantity
    order.m_action = action
    order.m_auxPrice = limit_price
    order.m_lmtPrice = limit_price
    return order


def readOrders():
    orders = []
    os.chdir(ordersPath)
    for ordersFileName in glob.glob("orders*.txt"):
        print("filename:"+ordersFileName)
        with open(ordersFileName) as fp:
            for line in fp:
                parts = line.split(',')
                order = MyOrder(parts[0], parts[1], float(parts[2]),int(parts[3]))
                orders.append(order)
            fp.close()
            os.remove(ordersFileName)
    return orders


def checkOrders():
    print("checking orders")
    orders = readOrders()
    if len(orders) > 0:
        processOrders(orders)
    else:
        print("no orders to process")
    tws_conn.reqAllOpenOrders()

def processOrders(orders):
    global order_id
    for order in orders:
        if order.type == "CAN" :
            cancelOrders(order.symbol)
            posted_orders.append(order)
            continue
        contract = create_contract(order.symbol, 'STK', 'SMART', 'SMART', 'USD')
        order_type = "LMT"
        order_action = order.type

        if order_action == "stopsell":
            order_type = "STP"
            order_action = "SELL"

        if order_action == "stopbuy":
            order_type = "STP"
            order_action = "BUY"

        new_order = create_order(order_type, order_action,order.amount, order.price)
        tws_conn.placeOrder(order_id, contract, new_order)
        posted_orders.append(order)
        order_id += 1


def cancelAllOrders():
    print ("Cancel all orders")
    global cancelAllOrdersSet
    cancelAllOrdersSet = True
    tws_conn.reqAllOpenOrders()

def cancelOrders(symbol):
    print ("requesting cancel "+symbol)
    symbolsToCancel.append(symbol)
    tws_conn.reqAllOpenOrders()

def checkOrdersForCancel(msg):
    symbol=msg.contract.m_symbol
    print("cancelling callback"+symbol)
    order=msg.order
    action=order.m_action
    if order.m_orderType == 'STP':
        action = 'STP'
    myorder=MyOrder(symbol,action,order.m_lmtPrice,order.m_totalQuantity)
    posted_orders.append(myorder)
    if cancelAllOrdersSet or symbol in symbolsToCancel:
        print("about to cancel:"+str(order.m_orderId))
        tws_conn.cancelOrder(order.m_orderId)
        print("canceled:"+symbol)


def readCurrentPosition(msg):
    print ("reading positions")
    currentPosition.append(msg)


def readPositionsFinished(msg):
    writePositionFile()


def writePositionFile():
    print ("write positions file")
    global currentPosition
    now=datetime.datetime.now()
    positionsFile = open('c:/lightspeeddata/lspdnames.txt', 'w')
    for position in currentPosition:
        positionsFile.write(position.contract.m_symbol   +','+
                            str(position.pos) + ',' +
                            str(position.avgCost) + '\n')
        # python will convert \n to os.linesep
    positionsFile.close()
    currentPosition=[]

def readOpenOrdersFinished(msg):
    global symbolsToCancel
    print ("read openorders finished")
    symbolsToCancel = []
    writeOpenOrdersFile()

def writeOpenOrdersFile():
    global  posted_orders
    postedOrdersFile = open('c:/lightspeeddata/lspdEntryNames.txt', 'w')
    for order in posted_orders:
        if order.type == 'STP':
            continue
        amount = order.amount
        print ("type:"+order.type)
        if order.type == 'SELL':
            amount = amount * -1
        postedOrdersFile.write(order.symbol + ',' +
                               str(amount) + ',' +
                               str(order.price) + '\n')
    # python will convert \n to os.linesep
    postedOrdersFile.close()
    posted_orders=[]


if __name__ == "__main__":
    tws_conn = Connection.create(port=7497, clientId=randint(0, 52214))
    tws_conn.register(error_handler, 'Error')
    tws_conn.register(checkOrdersForCancel,message.openOrder)
    tws_conn.register(readOpenOrdersFinished, message.openOrderEnd)
    tws_conn.register(readCurrentPosition,message.position)
    tws_conn.register(readPositionsFinished,message.positionEnd)

    tws_conn.registerAll(reply_handler)
    tws_conn.connect()
    schedule.every(15).seconds.do(checkOrders)
    schedule.every(5).seconds.do(tws_conn.reqPositions)


    while 1:
        now=datetime.datetime.now()
        if now.hour > 22 and not cancelOrdersReached:
            cancelOrdersReached = True
            print ("closing hour reached")
            cancelAllOrders()
        if now.hour < 22 :
            cancelOrdersReached = False
        schedule.run_pending()
        time.sleep(1)







